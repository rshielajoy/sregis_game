
let scorePoint = 0;
let timer = 15;


function startGame () {

    setInterval(randomShow, 750);
    //Setting time interval for the game
    setInterval(timeGame, 1000); 

    //Alternating buttons for stop and start
    let a = document.getElementById("start");  
    let b = document.getElementById("stop");
    a.style.display = "none";
    b.style.display = "block";
}

    //play onclick audio
function audioGame () {
    var pop = document.getElementById("sound");
    pop.play();
}


//generate random number of Objects
function randomObject () {
    var ran = Math.floor(Math.random() * 25) + 1;
    return ran; 
}
    
//random pop up of objects
function randomShow () {
    let temp;
    for (x = 1; x <= 24; x++)
    {
        temp = document.getElementById("object" + x);
        temp.style.display="none"; 
    }
    
    var x = document.getElementById("object" + randomObject());
    x.style.display = "block"; 
}

//set time
function timeGame () {
    timer--;
    document.getElementById("time").innerHTML = "Time: " + timer + " sec.";

    if (timer <= 0) {
        gameAlert();
    }
}

//promps alert message when time is equal to zero.
function gameAlert () {
    alert("Game Over! " + "Your score is " + scorePoint + ".");
    timer = 15;
    scorePoint = 0; 
    document.getElementById("time").innerHTML = "Time: " + timer + " sec.";
    document.getElementById("score").innerHTML = 0;
}

//clock is onClick
function addTime () {
    audioGame();
    timer += 5;
    document.getElementById("time").innerHTML = "Time: " + timer + " sec.";
}

//mole is onClick
function scorePlus() {
    audioGame();
    scorePoint += 3;
    document.getElementById("score").innerHTML = scorePoint;
}

//worm is onClick
function scoreMinus() {
    audioGame();
    scorePoint += 2;
    timer -= 2;
    document.getElementById("score").innerHTML = scorePoint;
    document.getElementById("time").innerHTML = "Time: " + timer + " sec.";
}

function stopGame () {
    location.reload(true);
}